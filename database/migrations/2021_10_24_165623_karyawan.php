<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Karyawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->char('nama_karyawan',255);
            $table->char('no_karyawan',15);
            $table->char('no_telp_karyawan',15);
            $table->char('jabatan_karyawan',255);
            $table->char('divisi_karyawan',255);
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
