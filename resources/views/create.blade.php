@extends('layouts.main')
@section('container')
    <section>
        <div class="container">
            <h3>Tambah Karyawan</h3>
            <div class="row">
                <div class="col-lg-8">
                    <form action="{{ url('/store') }}" method="POST">
                        @csrf <!-- untuk menyaring -->
                        <div class="form-group">
                            <label for="nama">Nama Karyawan</label>
                            <input type="text" name="nama_karyawan" class="form-control" placeholder="Masukan nama lengkap ">
                        </div>
                        <div class="form-group">
                            <label for="nama">No Karyawan</label>
                            <input type="text" name="no_karyawan" class="form-control" placeholder="Masukan no karyawan">
                        </div>
                        <div class="form-group">
                            <label for="nama">No Telepon </label>
                            <input type="text" name="no_telp_karyawan" class="form-control" placeholder="Masukan no telepon">
                        </div>
                        <div class="form-group">
                            <label for="nama">Jabatan</label>
                            <input type="text" name="jabatan_karyawan" class="form-control" placeholder="Masukan jabatan">
                        </div>
                        <div class="form-group">
                            <label for="nama">Divisi</label>
                            <input type="text" name="divisi_karyawan" class="form-control" placeholder="Masukan divisi">
                        </div>
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        <div class="form-group mt-2">
                           <a href="{{ url('/') }}">Kembali Ke Home</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
   