<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Karyawan</title>
    
    @stack('before-style')
    @include('includes.style')
    @stack('after-style')
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            @stack('before')
            @yield('content')
            @stack('after-content')
        </div>
    </div>
    @stack('before-style')
    @include('includes.style')
    @stack('after-style')
</body>
</html>