
   @extends('layouts.main')
   @section('container')
    <section>
       <div class="row">
         <h3>Daftar Karyawan</h3>
          <div class="col-lg-8 mt-4">
            <a href="{{url('create')}}" class="btn btn-warning">+ Tambah Daftar Karyawan</a>
          </div>
          <div class="col-lg-8 mt-3">
            <table class="table-bordered">
               <tr>
                  <th>No</th>
                  <th>Nama Karyawan</th>
                  <th>No Karyawan</th>
                  <th>No Telpon</th>
                  <th>Jabatan</th>
                  <th>Divisi</th>
                  <th>Action</th>

               </tr>
               @foreach ($data as $dataKaryawan)
               <tr>
                  <td>{{ $dataKaryawan->id }}</td>
                  <td>{{ $dataKaryawan->nama_karyawan }}</td>
                  <td>{{ $dataKaryawan->no_karyawan }}</td>
                  <td>{{ $dataKaryawan->no_telp_karyawan}}</td>
                  <td>{{ $dataKaryawan->jabatan_karyawan }}</td>
                  <td>{{ $dataKaryawan->divisi_karyawan }}</td>
                  <td>
                     <a href="{{ url('/show/'.$dataKaryawan->id) }}" class="btn btn-success">Update</a>
                     <a href="{{ url('/destroy/'.$dataKaryawan->id) }}" class="btn btn-danger">Delete</a>
                  </td>

               </tr>
               @endforeach
            </table>
          </div>
       </div>
    </section>
   @endsection

   
    
