<?php

use App\Http\Controllers\DataKaryawanController;
use App\Http\Controllers\DaftarController;
use App\Models\daftar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//cara route ke 1
Route::get('/', function () {
    return view('home',[
        "title"=>"Home",
        "alamat"=>"Alamat : Jl. Pajajaran Indah V No.7, RT.01/RW.11",
        "alamat2"=>"Baranangsiang, Kec. Bogor Tim., Kota Bogor, Jawa Barat 16143",
        "telepon"=>"Telepon : 0895-7042-64518",
        "service"=>"Service options : Dine-in · Takeaway · No-contact delivery",
        "text"=>"Traditional Indonesian comfort food & funky fruit teas in a contemporary coffeehouse.",
        "email"=>"officialkopinako@gmail.com",
        
    ]);
});
Route::get('/daftar', [DaftarController::class,'index']);


Route::get('/create', [DaftarController::class,'create']);
Route::post('/store',[DaftarController::class,'store']);
Route::get('/show/{id}', [DaftarController::class,'show']);
Route::post('/update/{id}',[DaftarController::class,'update']);
Route::get('/destroy/{id}', [DaftarController::class,'destroy']);




