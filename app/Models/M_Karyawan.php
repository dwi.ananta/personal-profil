<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class M_Karyawan extends Model
{
    use softDeletes;

    protected $table= 'karyawan';
    protected $fillable =[
        'nama_karyawan',
        'no_karyawan',
        'no_telp_karyawan',
        'jabatan_karyawan',
        'divisi_karyawan'
    ];
    protected $hidden;
}
